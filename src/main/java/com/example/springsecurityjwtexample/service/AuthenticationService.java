package com.example.springsecurityjwtexample.service;

import com.example.springsecurityjwtexample.dto.UserDto;
import com.example.springsecurityjwtexample.dto.UserRequest;
import com.example.springsecurityjwtexample.dto.UserResponse;
import com.example.springsecurityjwtexample.entity.User;
import com.example.springsecurityjwtexample.enums.Role;
import com.example.springsecurityjwtexample.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserRepository userRepository;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public UserResponse save(UserDto userdto) {
        User user = User.builder()
                .username(userdto.getUsername())
                .password(userdto.getPassword())
                .role(Role.USER).build();
        userRepository.save(user);
        var token = jwtService.generateToken(user);
        return UserResponse.builder().token(token).build();
    }

    public UserResponse auth(UserRequest userRequest) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userRequest.getUsername(), userRequest.getPassword()));
            User user = userRepository.findByUsername(userRequest.getUsername()).orElseThrow();
            String token = jwtService.generateToken(user);
            return UserResponse.builder().token(token).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
