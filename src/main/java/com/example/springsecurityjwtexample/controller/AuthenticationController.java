package com.example.springsecurityjwtexample.controller;

import com.example.springsecurityjwtexample.dto.UserDto;
import com.example.springsecurityjwtexample.dto.UserRequest;
import com.example.springsecurityjwtexample.dto.UserResponse;
import com.example.springsecurityjwtexample.entity.User;
import com.example.springsecurityjwtexample.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/login")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/save")
    public ResponseEntity<UserResponse> saveUser(@RequestBody UserDto userdto) {
        return ResponseEntity.ok(authenticationService.save(userdto));
    }

    @PostMapping("/auth")
    public ResponseEntity<UserResponse> auth(@RequestBody UserRequest userRequest) {
        return ResponseEntity.ok((authenticationService.auth(userRequest)));
    }
}
